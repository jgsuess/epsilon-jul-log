package net.jgsuess.epsilon.log.jul.util.ai.plastermind.tests;

public class Bravo {
	private String andorra;
	private String angola;
	
	public Bravo(String andorra, String angola) {
		super();
		this.andorra = andorra;
		this.angola = angola;
	}

	public String getAndorra() {
		return andorra;
	}

	public void setAndorra(String andorra) {
		this.andorra = andorra;
	}

	public String getAngola() {
		return angola;
	}

	public void setAngola(String angola) {
		this.angola = angola;
	}
}