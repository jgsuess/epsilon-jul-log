package net.jgsuess.epsilon.log.jul.util.ai.plastermind.tests;

public class Alpha {
	private String afghanistan;
	private String albania;
	
	public Alpha(String afghanistan, String albania) {
		super();
		this.afghanistan = afghanistan;
		this.albania = albania;
	}

	public String getAfghanistan() {
		return afghanistan;
	}

	public void setAfghanistan(String afghanistan) {
		this.afghanistan = afghanistan;
	}

	public String getAlbania() {
		return albania;
	}

	public void setAlbania(String albania) {
		this.albania = albania;
	}
}