package net.jgsuess.epsilon.log.jul.tests;

import static java.lang.System.err;
import static java.lang.System.out;
import static java.net.URI.create;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.logging.LogManager.getLogManager;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.eclipse.epsilon.eol.EolModule;
import org.eclipse.epsilon.eol.dt.ExtensionPointToolNativeTypeDelegate;
import org.eclipse.epsilon.eol.execute.context.IEolContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RunEOLTest {

	/**
	 *
	 * @throws Exception
	 */
	@BeforeEach
	public void setUpBeforeClass() throws Exception {

		/**
		 * Merge logging configuration
		 */
		final Function<String, BiFunction<String, String, String>> givePreferenceToNewSettings = (
				key) -> ((oldValue, newValue) -> newValue == null ? oldValue : newValue);
		final InputStream newLoggingConfiguration = RunEOLTest.class.getResourceAsStream("logging.properties");
		getLogManager().updateConfiguration(newLoggingConfiguration, givePreferenceToNewSettings);

	}

	@Test
	void test() throws Exception {

		final InputStream input = getClass().getResourceAsStream("usesLogger.eol");
		final String code = new String(input.readAllBytes(), UTF_8);

		final EolModule module = new EolModule();
//		module.parse(code, new File("/zorg/program.eol"));
		module.parse(create("platform:/plugin/net.jgsuess.epsilon.log.jul.tests/src/net/jgsuess/epsilon/log/jul/tests/usesLogger.eol"));

		final IEolContext context = module.getContext();
		context.setOutputStream(out);
		context.setErrorStream(err);
		context.getNativeTypeDelegates().add(new ExtensionPointToolNativeTypeDelegate());

		module.execute();
	}

}
