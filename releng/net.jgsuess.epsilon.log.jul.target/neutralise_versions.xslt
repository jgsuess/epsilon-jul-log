<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output omit-xml-declaration="no" indent="yes" />
   <xsl:strip-space elements="*" />
   <!--Identity template, provides default behavior that copies all content into the output -->
   <xsl:template match="@*|node()">
      <xsl:copy>
         <xsl:apply-templates select="@*|node()" />
      </xsl:copy>
   </xsl:template>
   <!--specific behaviour for the 'unit' node -->
   <xsl:template match="/target/locations/location/unit">
      <xsl:copy>
         <!-- First copy everything -->
         <xsl:apply-templates select="@*|node()" />
         <!-- Then overwrite the 'version' attribute -->
         <xsl:attribute name="version">
            <xsl:value-of select="'0.0.0'" />
         </xsl:attribute>
      </xsl:copy>
   </xsl:template>
</xsl:stylesheet>