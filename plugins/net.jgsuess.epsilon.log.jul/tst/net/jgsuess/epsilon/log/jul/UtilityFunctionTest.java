package net.jgsuess.epsilon.log.jul;

import static java.net.URI.create;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Array;
import java.net.URI;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.stream.Stream;

import org.eclipse.epsilon.eol.parse.Eol_EolParserRules.newExpression_return;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class UtilityFunctionTest {

	@ParameterizedTest
	@MethodSource("getUriToPathPairs")
	void testPlatformUriToSourceClassName(URI uri, String expectedPath) {
		Set<String> sourceClassNames = ModuleLogUtil.URI_TO_NAMES.apply(ModuleLogUtil.DEFAULT_URI_TO_NAME_MAPPERS, uri);

		assertEquals(1, sourceClassNames.size(), "Expect exactly one solution");

		final String sourceClassName = sourceClassNames.stream().findAny().get();

		assertEquals(expectedPath, sourceClassName);
	}

	private static Stream<Arguments> getUriToPathPairs() {
		return Stream.of(makeArgument("platform:/plugin/lalilu/foobar/com?boo", "foobar/com"),
				makeArgument("file://hostname/path/to/the%20file.txt?boo", "path/to/the file.txt"));
	}

	private static Arguments makeArgument(final String uri, final String path) {
		return Arguments.of(create(uri), path);
	}

	@Test
	void testUpdateRecord() throws Exception {
		final LogRecord logRecord = new LogRecord(Level.INFO, "boo");
		ModuleLogUtil.updateRecordWithEolDetail(logRecord, "some.class.name", "some.logger.name", false, 0);
	}
	
	@Test
	void testUpdateRecord2() throws Exception {
		final LogRecord logRecord = new LogRecord(Level.INFO, "boo");
		ModuleLogUtil.updateRecordWithEolDetail(logRecord, "some.class.name", "some.logger.name", true, 0);
	}
	
	@Test
	void testUpdateRecord3() throws Exception {
		final LogRecord logRecord = new LogRecord(Level.INFO, "boo");
		logRecord.setParameters(new Object[] {});
		ModuleLogUtil.updateRecordWithEolDetail(logRecord, "some.class.name", "some.logger.name", true, 0);
	}

}
