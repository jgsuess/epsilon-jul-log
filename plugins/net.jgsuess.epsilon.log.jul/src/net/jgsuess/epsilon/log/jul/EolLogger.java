package net.jgsuess.epsilon.log.jul;

import java.util.ResourceBundle;
import java.util.logging.Filter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class EolLogger extends Logger {

	final Logger delegate;
	
	final String sourceClassName;

	public ResourceBundle getResourceBundle() {
		return delegate.getResourceBundle();
	}

	public String getResourceBundleName() {
		return delegate.getResourceBundleName();
	}

	public void setFilter(Filter newFilter) throws SecurityException {
		delegate.setFilter(newFilter);
	}

	public Filter getFilter() {
		return delegate.getFilter();
	}

	public void setLevel(Level newLevel) throws SecurityException {
		delegate.setLevel(newLevel);
	}

	public Level getLevel() {
		return delegate.getLevel();
	}

	public boolean isLoggable(Level level) {
		return delegate.isLoggable(level);
	}

	public String getName() {
		return delegate.getName();
	}

	public void addHandler(Handler handler) throws SecurityException {
		delegate.addHandler(handler);
	}

	public void removeHandler(Handler handler) throws SecurityException {
		delegate.removeHandler(handler);
	}

	public Handler[] getHandlers() {
		return delegate.getHandlers();
	}

	public void setUseParentHandlers(boolean useParentHandlers) {
		delegate.setUseParentHandlers(useParentHandlers);
	}

	public boolean getUseParentHandlers() {
		return delegate.getUseParentHandlers();
	}

	public void setResourceBundle(ResourceBundle bundle) {
		delegate.setResourceBundle(bundle);
	}

	public Logger getParent() {
		return delegate.getParent();
	}

	public void setParent(Logger parent) {
		delegate.setParent(parent);
	}

	protected EolLogger(String loggerName, String sourceClassName, String resourceBundleName) {		
		super(loggerName, resourceBundleName);
		delegate = getLogger(loggerName, resourceBundleName);
		this.sourceClassName = sourceClassName;

	}

	public void log(LogRecord record) {
		record.setSourceClassName(sourceClassName);

		
		delegate.log(record);
	}

}
