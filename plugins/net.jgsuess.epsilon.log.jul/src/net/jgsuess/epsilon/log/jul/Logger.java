package net.jgsuess.epsilon.log.jul;

import static java.util.Arrays.asList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Set.of;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toUnmodifiableSet;
import static java.util.stream.StreamSupport.stream;

import java.net.URI;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.eclipse.epsilon.common.module.IModule;
import org.eclipse.epsilon.eol.execute.context.IEolContext;
import org.eclipse.epsilon.eol.tools.ITool;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public final class Logger implements ITool {

	@Setter
	@Getter
	private IEolContext context;

	@Setter
	@Getter
	private Map<String, Function<String, Path>> schemeToLoggerNameComputers = new HashMap<>();

	@Setter
	@Getter
	private Set<Function<URI, Optional<String>>> uriToSourceClassNameMappers;
	
	EolLogger delegate;
	
	public Logger() {
		setUriToSourceClassNameMappers(DEFAULT_URI_TO_SOURCE_CLASS_NAME_MAPPERS);
	}

	@Override
	/**
	 * TODO: Should this be an avenue of configuration for the Logger? What is most
	 * useful and idiomatic?
	 */
	public void initialize(List<Object> parameters) {
		// TODO: What is the resource bundle for this?
		final String loggerName = getLoggerName();
		
		final String sourceClassName = getSourceClassName();
		delegate = new EolLogger(loggerName, sourceClassName, null);
	}

	public String getLoggerName() {

		/*
		 * Get the name of the source class
		 */
		final String sourceClassName = getSourceClassName();

		/*
		 * Get function the function that derives the name
		 */
		
		final String scheme = getModuleUri().getScheme();
		
		/*
		 * Gets the function for the module URI scheme that applies to mapping this source class
		 * TODO: Maybe this should also be queried dynamically based on the URI rather than just the scheme.
		 */
		final Function<String, Path> loggerNameComputer = getSchemeToLoggerNameComputers().getOrDefault(scheme, s -> Path.of(""));

		/*
		 * Map the name of the source class to the name of the logger
		 */
		final Path loggerPath = loggerNameComputer.apply(sourceClassName);

		/*
		 * Add dots between components of the logger's name and concatenate
		 */
		final String loggerName = separatePathByDots(loggerPath);

		/*
		 * Return assembled the name
		 */
		return loggerName;
	}

	public static final String separatePathByDots(final Path path) {
		return stream(path.spliterator(), false).map(Path::toString).collect(joining("."));
	}

	/**
	 * Get the line where the statement that invoked the statement containing this
	 * call starts. Intended to provide an implicit means to line-stamp the log
	 * message.
	 * 
	 * @return
	 */
	public final int getLine() {
		return getContext().getFrameStack().getCurrentStatement().getRegion().getStart().getLine();
	}

	/**
	 * Computes the name of the source class based on stored mappings and the module
	 * uri.
	 *
	 * @return
	 */
	public String getSourceClassName() {

		final URI uri = getModuleUri();

		/**
		 * Based on the URI, there may be multiple source class names that are produced.
		 * This can be based on scheme, filename extension or any other factor.
		 */
		final Set<String> uriMappings = URI_TO_NAMES.apply(getUriToSourceClassNameMappers(), uri);

		final int numberOfMappings = uriMappings.size();

		switch (numberOfMappings) {
		case 0:
			throw new RuntimeException("No mapping to source class found for URI :" + uri);
		case 1:
			return uriMappings.iterator().next();
		default:
			throw new RuntimeException("Ambiguous mapping to source class found for URI :" + uri + " " + uriMappings);
		}

	}

	private URI getModuleUri() {
		final IModule module = getModule();

		final URI uri = module.getUri();

		return uri;
	}

	/**
	 * Given a URI scheme name yields a matching predicate for URIs with that
	 * scheme.
	 */
	public static final Function<String, Predicate<URI>> MATCHES_SCHEME = scheme -> uri -> uri.getScheme()
			.matches(scheme);

	private static final BiFunction<String, Function<URI, String>, Function<URI, Optional<String>>> IF_SCHEME_FITS = (
			scheme,
			uriToString) -> uri -> MATCHES_SCHEME.apply(scheme).test(uri) ? of(uriToString.apply(uri)) : empty();

	/**
	 * Maps an Eclipse Platform URI to a String. The String consists of path
	 * elements starting from index 2 inclusive. So
	 * <code>platform:/plugin/net.jgsuess.epsilon.log.jul/modules/eol/challenge.eol#something</code>
	 * yields <code>modules/eol/challenge.eol</code>.
	 */
	public static final Function<URI, Optional<String>> MAP_PLATFORM_URI = IF_SCHEME_FITS.apply("platform",
			uri -> getSubPath(3, uri));

	/**
	 * Maps a File URI to a String. The String consists of path elements starting
	 * from index 0 inclusive. So
	 * <code>file://some/where/over/the/rainbow.txt</code> yields
	 * <code>some/where/over/the/rainbow.txt</code>.
	 */
	public static final Function<URI, Optional<String>> MAP_FILE_URI = IF_SCHEME_FITS.apply("file",
			uri -> getSubPath(1, uri));

	/**
	 * The set of mapping functions from URI to String this Logger will use by
	 * default. In this implementation, these are {{@link #MAP_FILE_URI} and
	 * {@link #MAP_PLATFORM_URI}.
	 */
	public static final Set<Function<URI, Optional<String>>> DEFAULT_URI_TO_SOURCE_CLASS_NAME_MAPPERS = of(
			MAP_PLATFORM_URI, MAP_FILE_URI);

	/**
	 * Computes the set of Strings produced by applying a set of mapping functions
	 * to a URI.
	 */
	public static final BiFunction<Set<Function<URI, Optional<String>>>, URI, Set<String>> URI_TO_NAMES = (
			fs, u) -> fs.stream().map(f -> f.apply(u)).filter(Optional::isPresent).map(Optional::get)
					.collect(toUnmodifiableSet());

	private static final List<String> splitPath(final URI uri) {
		final String path = uri.getPath();
		return asList(path.split("/"));
	}

	private static final String getSubPath(final int fromIndex, URI uri) {
		return splitPath(uri).subList(fromIndex, splitPath(uri).size()).stream().collect(joining("/"));
	}

	private IModule getModule() {
		return getContext().getModule();
	}

	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	public void log(LogRecord record) {
		delegate.log(record);
	}

	public void log(Level level, String msg) {
		delegate.log(level, msg);
	}

	public void log(Level level, Supplier<String> msgSupplier) {
		delegate.log(level, msgSupplier);
	}

	public void log(Level level, String msg, Object param1) {
		delegate.log(level, msg, param1);
	}

	public void log(Level level, String msg, Object[] params) {
		delegate.log(level, msg, params);
	}

	public void log(Level level, String msg, Throwable thrown) {
		delegate.log(level, msg, thrown);
	}

	public void log(Level level, Throwable thrown, Supplier<String> msgSupplier) {
		delegate.log(level, thrown, msgSupplier);
	}

	public void logp(Level level, String sourceClass, String sourceMethod, String msg) {
		delegate.logp(level, sourceClass, sourceMethod, msg);
	}

	public void logp(Level level, String sourceClass, String sourceMethod, Supplier<String> msgSupplier) {
		delegate.logp(level, sourceClass, sourceMethod, msgSupplier);
	}

	public void logp(Level level, String sourceClass, String sourceMethod, String msg, Object param1) {
		delegate.logp(level, sourceClass, sourceMethod, msg, param1);
	}

	public void logp(Level level, String sourceClass, String sourceMethod, String msg, Object[] params) {
		delegate.logp(level, sourceClass, sourceMethod, msg, params);
	}

	public void logp(Level level, String sourceClass, String sourceMethod, String msg, Throwable thrown) {
		delegate.logp(level, sourceClass, sourceMethod, msg, thrown);
	}

	public void logp(Level level, String sourceClass, String sourceMethod, Throwable thrown,
			Supplier<String> msgSupplier) {
		delegate.logp(level, sourceClass, sourceMethod, thrown, msgSupplier);
	}

	public void logrb(Level level, String sourceClass, String sourceMethod, ResourceBundle bundle, String msg,
			Object... params) {
		delegate.logrb(level, sourceClass, sourceMethod, bundle, msg, params);
	}

	public void logrb(Level level, ResourceBundle bundle, String msg, Object... params) {
		delegate.logrb(level, bundle, msg, params);
	}

	public void logrb(Level level, String sourceClass, String sourceMethod, ResourceBundle bundle, String msg,
			Throwable thrown) {
		delegate.logrb(level, sourceClass, sourceMethod, bundle, msg, thrown);
	}

	public void logrb(Level level, ResourceBundle bundle, String msg, Throwable thrown) {
		delegate.logrb(level, bundle, msg, thrown);
	}

	public void entering(String sourceClass, String sourceMethod) {
		delegate.entering(sourceClass, sourceMethod);
	}

	public void entering(String sourceClass, String sourceMethod, Object param1) {
		delegate.entering(sourceClass, sourceMethod, param1);
	}

	public void entering(String sourceClass, String sourceMethod, Object[] params) {
		delegate.entering(sourceClass, sourceMethod, params);
	}

	public void exiting(String sourceClass, String sourceMethod) {
		delegate.exiting(sourceClass, sourceMethod);
	}

	public void exiting(String sourceClass, String sourceMethod, Object result) {
		delegate.exiting(sourceClass, sourceMethod, result);
	}

	public void throwing(String sourceClass, String sourceMethod, Throwable thrown) {
		delegate.throwing(sourceClass, sourceMethod, thrown);
	}

	public void severe(String msg) {
		delegate.severe(msg);
	}

	public void warning(String msg) {
		delegate.warning(msg);
	}

	public void info(String msg) {
		delegate.info(msg);
	}

	public void config(String msg) {
		delegate.config(msg);
	}

	public void fine(String msg) {
		delegate.fine(msg);
	}

	public void finer(String msg) {
		delegate.finer(msg);
	}

	public void finest(String msg) {
		delegate.finest(msg);
	}

	public void severe(Supplier<String> msgSupplier) {
		delegate.severe(msgSupplier);
	}

	public void warning(Supplier<String> msgSupplier) {
		delegate.warning(msgSupplier);
	}

	public void info(Supplier<String> msgSupplier) {
		delegate.info(msgSupplier);
	}

	public void config(Supplier<String> msgSupplier) {
		delegate.config(msgSupplier);
	}

	public void fine(Supplier<String> msgSupplier) {
		delegate.fine(msgSupplier);
	}

	public void finer(Supplier<String> msgSupplier) {
		delegate.finer(msgSupplier);
	}

	public void finest(Supplier<String> msgSupplier) {
		delegate.finest(msgSupplier);
	}

}
